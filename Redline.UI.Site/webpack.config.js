﻿"use strict";
{
    let _path = require('path');
    const _cleanBundle = require('clean-webpack-plugin');
    const _analyzeBundle = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    const _extractText = require('extract-text-webpack-plugin');

    const _extractLess = new _extractText({
        filename: "[name].[contentHash].css"
    });

    const bundleFolder = "wwwroot/bundle/";

    const analyzerConfig = {
        analyzerMode: 'static',
        analyzerHost: '127.0.0.1',
        analyzerPort: 8888,
        reportFilename: 'static-report.html',
        defaultSizes: 'parsed',
        openAnalyzer: true,
        generateStatsFile: false,
        statsFilename: 'stats.json',
        statsOptions: null,
        logLevel: 'info'
    };

    module.exports = {
        context: __dirname,
        devtool: "inline-source-map",
        entry: _path.resolve(__dirname, "Client/main.ts"),
        output: {
            filename: "scripts.[hash].js",
            path: _path.resolve(__dirname, bundleFolder),
        },
        module: {
            loaders: [
                {
                    test: /\.less$/,
                    use: _extractLess.extract({
                        use: [{
                            loader: "css-loader",
                            options: { sourceMap: true }
                        }, {
                            loader: "less-loader",
                            options: { sourceMap: true }
                        }]
                    })
                },
                {
                    test: /\.scss$/,
                    use: _extractLess.extract({
                        use: [{
                            loader: "css-loader",
                            options: { sourceMap: true }
                        }, {
                            loader: "sass-loader",
                            options: { sourceMap: true }
                        }]
                    })
                },
                {
                    test: /\.(tsx|ts)?$/,
                    loader: 'ts-loader',
                    exclude: /(node_modules|bower_components)/
                }
            ]
        },
        plugins: [
            new _cleanBundle([bundleFolder]),
            new _analyzeBundle(analyzerConfig),
            _extractLess
        ]
    };
}