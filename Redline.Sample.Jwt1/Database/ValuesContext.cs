﻿using Microsoft.EntityFrameworkCore;
using Redline.Sample.Jwt1.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redline.Sample.Jwt1
{
    public class ValuesContext : DbContext
    {
        public ValuesContext(DbContextOptions<ValuesContext> options)
            : base(options)
        {

        }

        public DbSet<ValueItem> ValueItems { get; set; }
    }
}
