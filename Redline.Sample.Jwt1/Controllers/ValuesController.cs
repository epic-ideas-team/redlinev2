﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Redline.Sample.Jwt1.Controllers
{
    public class ValueItem
    {
        public long Id { get; set; }

        public string Value { get; set; }
    }


    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        private readonly ValuesContext _context;

        public ValuesController(ValuesContext context)
        {
            _context = context;

            if (_context.ValueItems.Count() == 0)
            {
                _context.ValueItems.Add(new ValueItem { Value = "Value1" });

                _context.SaveChanges();
            }
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<ValueItem> Get()
        {
            return _context.ValueItems.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            var value = _context.ValueItems.FirstOrDefault(t => t.Id == id);

            if (value == null)
            {
                return NotFound();
            }
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]ValueItem value)
        {
            if (value == null)
            {
                return BadRequest();
            }

            _context.ValueItems.Add(value);

            _context.SaveChanges();

            return CreatedAtAction("Post", new { id = value.Id }, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var value = _context.ValueItems.FirstOrDefault(t => t.Id == id);

            if (value == null)
            {
                return NotFound();
            }

            _context.ValueItems.Remove(value);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
