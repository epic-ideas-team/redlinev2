﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace Redline.Api.Features.Values
{
    [Route("[controller]")]
    public class ValuesController : Controller
    {

        public readonly IHostingEnvironment Env;

        public ValuesController(IHostingEnvironment env)
        {
            Env = env;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("check-env")]
        public IActionResult TestEnv()
        {
            return Ok($"Is environment 'development' - {Env.IsDevelopment()}");
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
