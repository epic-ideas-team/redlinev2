﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Redline.Api2.Configuration;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc;
using MediatR;

namespace Redline.Api2
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions(Configuration);

            services.AddCorsPolicies();

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Redline Project",
                    Description = "Redline API Swagger surface"
                });
            });

            services.AddMediatR(typeof(Startup));

            services.AddWebApi(o =>
            {
                o.OutputFormatters.Remove(new XmlDataContractSerializerOutputFormatter());
                o.UseCentralRoutePrefix(new RouteAttribute("api/v{version}"));
            });

            services.AddCompositionRoot();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("ALLOWANY");

            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "Redline Project API v1");
            });

            app.UseMvc();
        }
    }
}
